Sphinx-docs-template
--------------------


This is a stub documentation project that uses sphinx for creating documentation pages.
This is a Python project, but can be used for any type of projects, not even software.

Dependencies:
- Read the docs theme
- PlantUML for model illustration
- Napoleon for Google and Numpy style Python docstrings

Installation:

```
  git clone https://gitlab.com/npolar/sphinx-docs-template
  cd sphinx-docs-template
  pip install -r requirements.txt
```

Plantuml is an optional dependency, can be installed via:

```
  apt-get install plantuml
```

Add documentation
=================



Build
=====
To build documentation pages as html, do:
```
  make html
```
The pages will be created in the `source` directory
